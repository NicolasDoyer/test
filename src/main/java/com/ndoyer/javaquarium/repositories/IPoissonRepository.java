package com.ndoyer.javaquarium.repositories;

import org.springframework.data.jpa.repository.JpaRepository;
import com.ndoyer.javaquarium.DO.PoissonDO;

public interface IPoissonRepository extends JpaRepository<PoissonDO, Integer>{
	
	
}
