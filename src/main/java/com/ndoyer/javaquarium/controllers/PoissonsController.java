package com.ndoyer.javaquarium.controllers;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ndoyer.javaquarium.services.IPoissonService;
import com.ndoyer.javaquarium.DTO.PoissonDTO;
import com.ndoyer.javaquarium.DTO.PoissonPostDTO;

/**
 * Poisson controller
 * @author Nicolas
 *
 */
@Resource
@RestController
@RequestMapping(value = "/poissons")
public class PoissonsController{
	
	@Autowired
	private IPoissonService poissonService;
	
	/**
	 * Get a list of poissons
	 * @return List of poissons
	 */
	@RequestMapping(method = RequestMethod.GET)
	public List<PoissonDTO> getList() {
		return this.poissonService.getList();
	}

	/**
	 * Create a new poisson
	 * @param poisson
	 */
	@RequestMapping(method = RequestMethod.POST)
	public void createPoisson(@RequestBody final PoissonPostDTO poisson) {
		this.poissonService.createPoisson(poisson);
	}
}
