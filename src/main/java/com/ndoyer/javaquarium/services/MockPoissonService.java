package com.ndoyer.javaquarium.services;

import java.util.ArrayList;
import java.util.List;

import org.springframework.stereotype.Component;

import com.ndoyer.javaquarium.DTO.PoissonDTO;

public class MockPoissonService{

	public List<PoissonDTO> getList() {
		final List<PoissonDTO> result = new ArrayList<>();
		result.add(new PoissonDTO(0, "truc", "description ...", "red", "10 x 600", 10.60));
		result.add(new PoissonDTO(10, "machin", "description de merde", "brown", "80 x 50", 600.60));
		return result;
	}

}
