package com.ndoyer.javaquarium.services;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import com.ndoyer.javaquarium.repositories.IPoissonRepository;
import com.ndoyer.javaquarium.DO.PoissonDO;
import com.ndoyer.javaquarium.DTO.PoissonDTO;
import com.ndoyer.javaquarium.DTO.PoissonPostDTO;
import com.ndoyer.javaquarium.mappers.PoissonMapper;

/**
 * @author Nicolas Doyer
 */
@Component
public class PoissonService implements IPoissonService{

	@Autowired
	private IPoissonRepository poissonRepository;
	
	/**
	 * {@inheritDoc}
	 */
	public List<PoissonDTO> getList() {
		
		final List<PoissonDO> poissons = this.poissonRepository.findAll();
		return PoissonMapper.mapToPoissonsDTO(poissons);
	}
	
	/**
	 * {@inheritDoc}
	 */
	public void createPoisson(PoissonPostDTO poisson) {
		this.poissonRepository.save(PoissonMapper.mapToPoissonDO(poisson));
	}
}
