package com.ndoyer.javaquarium.services;

import java.util.List;

import com.ndoyer.javaquarium.DTO.PoissonDTO;
import com.ndoyer.javaquarium.DTO.PoissonPostDTO;

public interface IPoissonService {

	/**
	 * Get a list of fishon
	 * @return
	 */
	public List<PoissonDTO> getList();
	
	/**
	 * Create a poisson
	 * @param poisson
	 */
	public void createPoisson(final PoissonPostDTO poisson);
}
