package com.ndoyer.javaquarium.mappers;

import java.util.ArrayList;
import java.util.List;

import com.ndoyer.javaquarium.DO.PoissonDO;
import com.ndoyer.javaquarium.DTO.PoissonDTO;
import com.ndoyer.javaquarium.DTO.PoissonPostDTO;

public class PoissonMapper {

	/**
	 * Transform a PoissontPostDTO to a PoissonDO
	 * @param poisson
	 * @return
	 */
	public static PoissonDO mapToPoissonDO(final PoissonPostDTO poisson) {
		final PoissonDO result = new PoissonDO();
		result.setSpecies_name(poisson.getName());
		result.setMain_colour(poisson.getColor());
		result.setLenght(poisson.getLength());		
		result.setWidth(poisson.getWidth());
		result.setPrice(poisson.getPrice());
		
		// Splitting description
		final List<String> descriptions = splitDescription(poisson.getDescription());
		
		try {
			result.setDescription_1(descriptions.get(0));
		} catch(IndexOutOfBoundsException exception) {
			result.setDescription_1(null);
		}
		
		try {
			result.setDescription_2(descriptions.get(1));
		} catch(IndexOutOfBoundsException exception) {
			result.setDescription_2(null);
		}
		
		try {
			result.setDescription_3(descriptions.get(2));
		} catch(IndexOutOfBoundsException exception) {
			result.setDescription_3(null);
		}
	
		return result;
	}
	
	/**
	 * Map multiple PoissonDO to PoissonDTO
	 * @param poissons
	 * @return
	 */
	public static List<PoissonDTO> mapToPoissonsDTO(final List<PoissonDO> poissons) {
		final List<PoissonDTO> list = new ArrayList<>();
		poissons.forEach(poisson -> {
			
			String description = "";
			if(poisson.getDescription_1() != null) {
				description += poisson.getDescription_1();
			}
			if(poisson.getDescription_2() != null) {
				description += '\n' + poisson.getDescription_2();
			}
			if(poisson.getDescription_3() != null) {
				description += poisson.getDescription_3();
			}
			
			list.add(
					new PoissonDTO(
							poisson.getID(),
							poisson.getSpecies_name(),
							description,
							poisson.getMain_colour(),
							poisson.getLenght() + " x " + poisson.getWidth(),
							poisson.getPrice()
					)
			);
		});
		return list;
	}
	
	/**
	 * Split poisson description every 255 characters
	 * @param description
	 * @return
	 */
	private static List<String> splitDescription(final String description) {
		final List<String> strings = new ArrayList<String>();
		int index = 0;
		while (index < description.length()) {
		    strings.add(description.substring(index, Math.min(index + 255,description.length())));
		    index += 255;
		}
		return strings;
	}
}
