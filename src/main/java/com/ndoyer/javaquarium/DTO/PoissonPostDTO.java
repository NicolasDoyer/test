package com.ndoyer.javaquarium.DTO;

public class PoissonPostDTO {

	private String name;
	
	private String description;
	
	private String color;
	
	private double length;
	
	private double width;
	
	private double price;
	
	public PoissonPostDTO() {
		super();
	}

	public PoissonPostDTO(final String name, final String description, final String color, final double length, final double width, final double price) {
		super();
		this.name = name;
		this.description = description;
		this.color = color;
		this.length = length;
		this.width = width;
		this.price = price;
	}
	
	public String getName() {
		return name;
	}

	public void setName(final String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(final String description) {
		this.description = description;
	}

	public String getColor() {
		return color;
	}

	public void setColor(final String color) {
		this.color = color;
	}

	public double getLength() {
		return length;
	}

	public void setLength(final double length) {
		this.length = length;
	}

	public double getWidth() {
		return width;
	}

	public void setWidth(final double width) {
		this.width = width;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(final double price) {
		this.price = price;
	}
	
}
