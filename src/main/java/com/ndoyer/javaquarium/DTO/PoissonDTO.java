package com.ndoyer.javaquarium.DTO;

/**
 * @author Nicolas
 */
public class PoissonDTO {

	public  PoissonDTO() {
		super();
	}
	
	/**
	 * Default constructor
	 * @param ID
	 * @param name
	 * @param description
	 * @param color
	 * @param dimensions
	 * @param price
	 */
	public PoissonDTO(final long ID, final String name, final String description, final String color, final String dimensions, final double price) {
		super();
		this.ID = ID;
		this.name = name;
		this.description = description;
		this.color = color;
		this.dimensions = dimensions;
		this.price = price;
	}

	private long ID;
	private String name;
	private String description;
	private String color;
	private String dimensions;
	private double price;
	
	/**
	 * @return the iD
	 */
	public long getID() {
		return ID;
	}
	
	/**
	 * @param iD the iD to set
	 */
	public void setID(final long iD) {
		ID = iD;
	}
	
	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}
	
	/**
	 * @param name the name to set
	 */
	public void setName(final String name) {
		this.name = name;
	}
	
	/**
	 * @return the description
	 */
	public String getDescription() {
		return description;
	}
	
	/**
	 * @param description the description to set
	 */
	public void setDescription(final String description) {
		this.description = description;
	}
	
	/**
	 * @return the colour
	 */
	public String getColour() {
		return color;
	}
	
	/**
	 * @param colour the color to set
	 */
	public void setColour(final String colour) {
		this.color = colour;
	}
	
	/**
	 * @return the dimensions
	 */
	public String getDimensions() {
		return dimensions;
	}
	
	/**
	 * @param dimensions the dimensions to set
	 */
	public void setDimensions(final String dimensions) {
		this.dimensions = dimensions;
	}
	
	/**
	 * @return the price
	 */
	public double getPrice() {
		return price;
	}
	
	/**
	 * @param price the price to set
	 */
	public void setPrice(final double price) {
		this.price = price;
	}
}
