package com.ndoyer.javaquarium.DO;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * Represent Poisson Data Object
 * @author Nicolas
 *
 */

@Entity
@Table(name = "P_POISSONS")
public class PoissonDO {
	
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column(name="P_ID")
	private long ID;
	
	@Column(name="P_ESPECE")
	private String species_name;

	@Column(name="P_DESC1")
	private String description_1;
	
	@Column(name="P_DESC2")
	private String description_2;
	
	@Column(name="P_DESC3")
	private String description_3;
	
	@Column(name="P_COULEUR")
	private String main_colour;
	
	@Column(name="P_LARGEUR")
	private double width;
	
	@Column(name="P_LONGUEUR")
	private double lenght;
	
	@Column(name="P_PRIX")
	private double price;
	
	public PoissonDO() {
		
	}
	
	public PoissonDO(
			final long ID,
			final String species_name,
			final String description_1,
			final String description_2,
			final String description_3,
			final String main_colour,
			final double width,
			final double height,
			final double length,
			final double price
			) {
		this.ID = ID;
		this.species_name = species_name;
		this.description_1 = description_1;
		this.description_2 = description_2;
		this.description_3 = description_3;
		this.main_colour = main_colour;
		this.width = width;
		this.lenght = length;
		this.price = price;
	}
	
	public double getPrice() {
		return price;
	}
	
	public void setPrice(final double price) {
		this.price = price;
	}
	
	public double getWidth() {
		return width;
	}
	
	public void setWidth(final double width) {
		this.width = width;
	}
	
	public double getLenght() {
		return lenght;
	}
	
	public void setLenght(final double lenght) {
		this.lenght = lenght;
	}
	
	public String getMain_colour() {
		return main_colour;
	}
	
	public void setMain_colour(final String main_colour) {
		this.main_colour = main_colour;
	}
	
	public String getDescription_3() {
		return description_3;
	}
	
	public void setDescription_3(final String description_3) {
		this.description_3 = description_3;
	}
	
	public String getDescription_2() {
		return description_2;
	}
	
	public void setDescription_2(final String description_2) {
		this.description_2 = description_2;
	}
	
	public String getDescription_1() {
		return description_1;
	}
	
	public void setDescription_1(final String description_1) {
		this.description_1 = description_1;
	}
	
	public String getSpecies_name() {
		return species_name;
	}
	
	public void setSpecies_name(final String species_name) {
		this.species_name = species_name;
	}

	public long getID() {
		return ID;
	}

	public void setID(final long iD) {
		ID = iD;
	}
}
