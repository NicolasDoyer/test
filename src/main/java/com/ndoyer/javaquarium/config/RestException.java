package com.ndoyer.javaquarium.config;

import java.util.LinkedList;
import java.util.List;

/**
 * Exception métier .
 * <br>Sera parser en JSON pour accompagner un webservice répondant une erreur (Http status Code 500).
 *
 * @author MaxD
 *
 */
public abstract class RestException extends Exception {

	private static final long serialVersionUID = -1369321981547646805L;

	/**
	 * Identifiant unique pour les exceptions métier.
	 */
	private ExceptionEnum businessCode;

	/**
	* Liste des paramètres de l'exceptions
	*/
	private List<String> attributs;

	/**
	 * @return the attributs
	 */
	public List<String> getAttributs() {
		return attributs;
	}

	/**
	 * @param attributs the attributs to set
	 */
	public void setAttributs(final List<String> attributs) {
		this.attributs = attributs;
	}

	/**
	 * Ajout d'un attribut
	 * @param attr
	 */
	public void addAttribut(final String attr) {
		// 1. Instanciation au premier appel
		if (getAttributs() == null) {
			setAttributs(new LinkedList<String>());
		}
		getAttributs().add(attr);
	}

	/**
	 * @return the businessCode
	 */
	public ExceptionEnum getBusinessCode() {
		return businessCode;
	}

	/**
	 * @param businessCode the businessCode to set
	 */
	public void setBusinessCode(final ExceptionEnum businessCode) {
		this.businessCode = businessCode;
	}

}
