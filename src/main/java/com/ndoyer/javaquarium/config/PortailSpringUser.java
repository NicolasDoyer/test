package com.ndoyer.javaquarium.config;

import java.util.Collection;
import java.util.Set;
import java.util.TreeSet;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.User;

/**
 * Extends du user spring pour rajouter des informations
 * @author Max
 *
 */
public class PortailSpringUser extends User {

	private static final long serialVersionUID = -2836522345185404025L;

	private Long id;
	private Boolean actif;
	private Boolean supprime;
	private Boolean admin;
	private Set<Long> idStructures;

	/**
	 * @param username
	 * @param password
	 * @param enabled
	 * @param accountNonExpired
	 * @param credentialsNonExpired
	 * @param accountNonLocked
	 * @param authorities
	 */
	public PortailSpringUser(final Long id, final String username, final String password, final boolean enabled, final boolean accountNonExpired,
			final boolean credentialsNonExpired, final boolean accountNonLocked, final Collection<? extends GrantedAuthority> authorities) {
		super(username, password, enabled, accountNonExpired, credentialsNonExpired, accountNonLocked, authorities);
		setId(id);
		setActif(Boolean.TRUE);
		setAdmin(Boolean.FALSE);
		setSupprime(Boolean.FALSE);
		setIdStructures(new TreeSet<Long>());
	}

	/**
	 * @return the id
	 */
	public Long getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(final Long id) {
		this.id = id;
	}

	/**
	 * @return the actif
	 */
	public Boolean getActif() {
		return actif;
	}

	/**
	 * @param actif the actif to set
	 */
	public void setActif(final Boolean actif) {
		this.actif = actif;
	}

	/**
	 * @return the supprime
	 */
	public Boolean getSupprime() {
		return supprime;
	}

	/**
	 * @param supprime the supprime to set
	 */
	public void setSupprime(final Boolean supprime) {
		this.supprime = supprime;
	}

	/**
	 * @return the admin
	 */
	public Boolean getAdmin() {
		return admin;
	}

	/**
	 * @param admin the admin to set
	 */
	public void setAdmin(final Boolean admin) {
		this.admin = admin;
	}

	/**
	 * @return the idStructures
	 */
	public Set<Long> getIdStructures() {
		return idStructures;
	}

	/**
	 * @param idStructures the idStructures to set
	 */
	public void setIdStructures(final Set<Long> idStructures) {
		this.idStructures = idStructures;
	}

}
